package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
    	 int mid = (lista.length - 1)/2;
    	 Comparable[] izq = new Comparable[mid];
    	 Comparable[] der = new Comparable[mid];
    	 for( int i = 0; i <= mid - 1; i++)
    	 {
    		 izq[i] = lista[i];
    	 }
    	 for( int j = 0; j <= mid - 1; j++)
    	 {
    		 der[j] = lista[j + mid];
    	 }
    	 merge_sort(izq, orden);
    	 merge_sort(der, orden);
    	 	 
    	 if( (izq.length == 2) && (der.length == 2)) return merge(izq, der, orden);
    	 return merge(izq, der, orden);
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {
    	// Trabajo en Clase 
    	 Comparable[] aux = new Comparable[izquierda.length + derecha.length];
    	 Comparable[] resp = new Comparable[aux.length];
    	 int i = 0;
    	 int mid = (aux.length/2);
    	 int j = mid + 1;
    	 for( int k = 0; k <= izquierda.length - 1; k++)
    	 {
    		 aux[k] = izquierda[k];
    	 }
    	 for( int l = 0; l <= derecha.length - 1; l++)
    	 {
    		 aux[izquierda.length + 1] = derecha[l];
    	 }
    	 for( int � = 0; � <= aux.length - 1; �++)
    	 {
    		 if( i > mid) resp[�] = aux[j++];
    		 else if( j > aux.length - 1) resp[�] = aux[i++];
    		 else if( aux[j].compareTo(aux[i]) < 0) resp[�] = aux[j++];
    		 else resp[�] = aux[i++];
    	 }
    	 
    	 return resp;
     }


}
